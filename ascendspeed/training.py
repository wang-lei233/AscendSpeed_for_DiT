import time
import torch
from functools import wraps

from megatron.core import mpu
from megatron.core.utils import get_model_config, get_model_type
from megatron.core.enums import ModelType

from megatron.training.checkpointing import save_checkpoint

from megatron.training.initialize import initialize_megatron
from megatron.training.initialize import set_jit_fusion_options

from megatron.training.training import setup_model_and_optimizer
from megatron.training.training import build_train_valid_test_data_iterators
from megatron.training.training import train
from megatron.training.training import evaluate_and_print_results
from megatron.training.training import print_datetime

from megatron.training.utils import (
    calc_params_l2_norm,
    check_adlr_autoresume_termination,
    is_last_rank,
    print_rank_0,
    print_rank_last,
    report_memory,
    unwrap_model)
from megatron.training.global_vars import (
    get_args,
    get_signal_handler,
    get_timers,
    get_tensorboard_writer,
    get_wandb_writer,
    get_one_logger,
    get_current_global_batch_size,
    get_num_microbatches,
    update_num_microbatches)
_TRAIN_START_TIME = time.time()


def pretrain(train_valid_test_dataset_provider,
             model_provider,
             model_type,
             forward_step_func,
             process_non_loss_data_func=None,
             extra_args_provider=None,
             args_defaults={}):

    # Initalize and get arguments, timers, and Tensorboard writer.
    initialize_megatron(extra_args_provider=extra_args_provider,
                        args_defaults=args_defaults)

    args = get_args()
    timers = get_timers()

    if args.log_progress:
        append_to_progress_log("Starting job")

    # Set pytorch JIT layer fusion options and warmup JIT functions.
    set_jit_fusion_options()

    # Adjust the startup time so it reflects the largest value.
    # This will be closer to what scheduler will see (outside of
    # image ... launches.
    global _TRAIN_START_TIME
    start_time_tensor = torch.npu.FloatTensor([_TRAIN_START_TIME])
    torch.distributed.all_reduce(start_time_tensor,
                                 op=torch.distributed.ReduceOp.MIN)
    _TRAIN_START_TIME = start_time_tensor.item()
    print_rank_0('time to initialize megatron (seconds): {:.3f}'.format(
        time.time() - _TRAIN_START_TIME))
    print_datetime('after megatron is initialized')

    args = get_args()
    timers = get_timers()

    one_logger = get_one_logger()
    if one_logger:
        one_logger.log_metrics({
            'train_iterations_warmup': 5
        })

    # Model, optimizer, and learning rate.
    timers('model-and-optimizer-setup', log_level=0).start(barrier=True)
    model, optimizer, opt_param_scheduler = setup_model_and_optimizer(
        model_provider, model_type)

    timers('model-and-optimizer-setup').stop()
    print_datetime('after model, optimizer, and learning rate '
                   'scheduler are built')
    config = get_model_config(model[0])

    # Data stuff.
    timers('train/valid/test-data-iterators-setup', log_level=0).start(
        barrier=True)
    if args.virtual_pipeline_model_parallel_size is not None:
        train_data_iterator = []
        valid_data_iterator = []
        test_data_iterator = []
        for i in range(len(model)):
            mpu.set_virtual_pipeline_model_parallel_rank(i)
            iterators = build_train_valid_test_data_iterators(
                train_valid_test_dataset_provider)
            train_data_iterator.append(iterators[0])
            valid_data_iterator.append(iterators[1])
            test_data_iterator.append(iterators[2])
    else:
        train_data_iterator, valid_data_iterator, test_data_iterator \
            = build_train_valid_test_data_iterators(
                train_valid_test_dataset_provider)
    timers('train/valid/test-data-iterators-setup').stop()
    print_datetime('after dataloaders are built')

    # Print setup timing.
    print_rank_0('done with setup ...')
    timers.log(['model-and-optimizer-setup',
                'train/valid/test-data-iterators-setup'], barrier=True)

    if not args.skip_train:
        print_rank_0('training ...')

        if args.dataloader_type == 'cyclic' and args.retro_project_dir:
            assert args.retro_cyclic_train_iters is not None
            args.train_iters = args.retro_cyclic_train_iters
            print_rank_0("retro cyclic train iters : %d" % args.train_iters)

        iteration = 0
        if args.do_train and args.train_iters > 0:
            iteration, num_floating_point_operations_so_far = train(
                forward_step_func,
                model, optimizer, opt_param_scheduler,
                train_data_iterator, valid_data_iterator,
                process_non_loss_data_func, config)

        print_datetime('after training is done')

        if args.save and iteration != 0 and iteration % args.save_interval != 0:
            save_checkpoint(iteration, model, optimizer, opt_param_scheduler,
                            num_floating_point_operations_so_far)
    else:
        print_rank_0('skipping training (--skip-train is on) ...')

        iteration = args.iteration

    if args.do_valid:
        prefix = f'iteration {iteration} on validation set'
        evaluate_and_print_results(prefix, forward_step_func,
                                   valid_data_iterator, model,
                                   iteration, process_non_loss_data_func, config,
                                   verbose=True, write_to_tensorboard=not args.skip_train)

    if args.do_test:
        prefix = f'iteration {iteration} on test set'
        evaluate_and_print_results(prefix, forward_step_func,
                                   test_data_iterator, model,
                                   iteration, process_non_loss_data_func, config,
                                   verbose=True, write_to_tensorboard=not args.skip_train)

def forward_step_wrapper(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        res = fn(*args, **kwargs)
        if(get_model_type(args[2]) == ModelType.dit_block):
            if mpu.is_pipeline_last_stage(ignore_virtual=True):
                return res
            assert isinstance(res, list) and len(res) == 1
            res = res[0]
            assert isinstance(res, list) and len(res) == 4
        return res
    return wrapper


def backward_step_wrapper(fn):
    @wraps(fn)
    def wrapper(input_tensor, output_tensor, output_tensor_grad, model_type, config):
        if(model_type == ModelType.dit_block):
            if config.timers is not None:
                config.timers('backward-compute', log_level=2).start()
            if not isinstance(input_tensor, list):
                input_tensor = [input_tensor]
            if not isinstance(output_tensor_grad, list):
                output_tensor_grad = [output_tensor_grad]
            if not isinstance(output_tensor, list):
                output_tensor = [output_tensor]

            if input_tensor[0] is not None:
                input_tensor[0].retain_grad()

            # Backward pass.
            if output_tensor_grad[0] is None and config.grad_scale_func is not None:
                output_tensor[0] = config.grad_scale_func(output_tensor[0])

            if config.deallocate_pipeline_outputs:
                from megatron.core.pipeline_parallel.schedules import custom_backward
                custom_backward(output_tensor[0], output_tensor_grad[0])
            else:
                torch.autograd.backward(output_tensor[0], grad_tensors=output_tensor_grad[0])

            # Collect the grad of the input_tensor.
            args = get_args()
            input_tensor_grad = [
                torch.empty((args.micro_batch_size,(args.image_size//8)**2 // args.patch_size**2,args.hidden_size), dtype = args.params_dtype , device = torch.cuda.current_device()),
                torch.empty((args.micro_batch_size,args.hidden_size), dtype = args.params_dtype , device = torch.cuda.current_device()),
                torch.empty((args.micro_batch_size), dtype = args.params_dtype , device = torch.cuda.current_device()),
                torch.empty((args.micro_batch_size,4,args.image_size//8,args.image_size//8), dtype = args.params_dtype , device = torch.cuda.current_device())
            ]
            if input_tensor[0] is not None:
                input_tensor_grad[0]=input_tensor[0].grad

            if config.timers is not None:
                config.timers('backward-compute').stop()

            return input_tensor_grad
        else:
            return fn(input_tensor, output_tensor, output_tensor_grad, model_type, config)
    return wrapper


def get_tensor_shapes_wrapper(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if(kwargs['model_type'] == ModelType.dit_block):
            args = get_args()
            res = []
            res.append((args.micro_batch_size,(args.image_size//8)**2 // args.patch_size**2,args.hidden_size))
            res.append((args.micro_batch_size,args.hidden_size))
            res.append((args.micro_batch_size))
            res.append((args.micro_batch_size,4,args.image_size//8,args.image_size//8))
        else:
            res = fn(*args, **kwargs)
        return res
    return wrapper