# Copyright (c) 2024, NVIDIA CORPORATION. All rights reserved.

from typing import List
from functools import wraps
import torch

from megatron.core.utils import get_model_config
from ascendspeed.arguments import process_args
from megatron.core.enums import ModelType
from megatron.core.utils import get_model_type

def extra_args_provider_decorator(extra_args_provider):
    @wraps(extra_args_provider)
    def wrapper(parser):
        if extra_args_provider is not None:
            parser = extra_args_provider(parser)
        parser = process_args(parser)
        return parser
    return wrapper

def finalize_model_grads_wrapper(fn):
    @wraps(fn)
    def wrapper(model: List[torch.nn.Module]):
        if(get_model_type(model[0]) == ModelType.dit_block):
            config = get_model_config(model[0])
            # All-reduce / reduce-scatter across DP replicas.
            if config.timers is not None:
                config.timers('all-grads-sync', log_level=1).start(barrier=config.barrier_with_L1_time)
            for model_chunk in model:
                model_chunk.finish_grad_sync()
            if config.timers is not None:
                config.timers('all-grads-sync').stop()
        else:
            fn(model)
    return wrapper