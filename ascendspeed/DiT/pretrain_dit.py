# Copyright (c) 2022, NVIDIA CORPORATION.  All rights reserved.

"""Pretrain VIT"""

import torch
import numpy as np
import ascendspeed.megatron_adaptor
from functools import partial
from megatron.training import get_args, get_timers, print_rank_0
from megatron.core.enums import ModelType
from megatron.core import mpu
from megatron.legacy.data.vit_dataset import build_train_valid_datasets
from megatron.training import pretrain
from megatron.training.utils import average_losses_across_data_parallel_group
from megatron.training.arguments import core_transformer_config_from_args
from ascendspeed.DiT import DiTConfig
from ascendspeed.DiT.models import DiT
from diffusers.models import AutoencoderKL
from ascendspeed.DiT.diffusion import create_diffusion
from ascendspeed.DiT.diffusion.gaussian_diffusion import LossType
from torchvision import transforms
from megatron.legacy.data.image_folder import ImageFolder
from megatron.legacy.data.data_samplers import RandomSeedDataset
from PIL import Image

diffusion = create_diffusion(timestep_respacing="")  # default: 1000 steps, linear noise schedule

dataiterator_dict = {}

def model_provider(pre_process=True, post_process=True):
    """Build the model."""

    args = get_args()
    config = core_transformer_config_from_args(args, DiTConfig)
    assert args.image_size % 8 == 0, "Image size must be divisible by 8 (for the VAE encoder)."
    config.latent_size = args.image_size // 8
    model = DiT(
        config=config,
        timestep_map=diffusion.timestep_map,
        original_num_steps=diffusion.original_num_steps,
        pre_process=pre_process,
        post_process=post_process
    )
    
    return model

def get_batch_on_this_tp_rank(data_iterator):

    args = get_args()

    def _broadcast(item):
        if item is not None:
           torch.distributed.broadcast(item, mpu.get_tensor_model_parallel_src_rank(), group=mpu.get_tensor_model_parallel_group())

    if mpu.get_tensor_model_parallel_rank() == 0:

        if data_iterator is not None:
            data = next(data_iterator)
        else:
            data = None
        batch = {
            'images': data[0].cuda(non_blocking = True),
            'labels': data[1].cuda(non_blocking = True)
        }
        _broadcast(batch['images'])
        _broadcast(batch['labels'])

    else:
        images=torch.empty((args.micro_batch_size,args.color_dim,args.image_size,args.image_size), dtype = torch.float32 , device = torch.cuda.current_device())
        labels=torch.empty((args.micro_batch_size), dtype = torch.int64 , device = torch.cuda.current_device())
        _broadcast(images)
        _broadcast(labels)
       
        batch = {
           'images': images,
           'labels': labels
        }

    return batch


def get_batch(data_iterator):
    """Build the batch."""
    if (mpu.is_pipeline_first_stage() or mpu.is_pipeline_last_stage()):
        batch = get_batch_on_this_tp_rank(data_iterator)
        x_start = batch['images']
        y = batch['labels']
        vae = AutoencoderKL.from_pretrained(f"../vae").to(torch.cuda.current_device())
        with torch.no_grad():
            x_start = vae.encode(x_start).latent_dist.sample().mul_(0.18215)
    else:
        x_start=None
        y=None
    return x_start, y


def loss_func(x_start, x_t, t, y, noise, output_tensor):
    model_kwargs = dict(y=y)
    loss_dict = diffusion.training_losses(output_tensor[0], x_start, x_t, t, model_kwargs, noise)
    loss = loss_dict["loss"].mean()

    averaged_loss = average_losses_across_data_parallel_group([loss])

    return loss, {"loss": averaged_loss[0]}


def center_crop_arr(pil_image, image_size):
    """
    Center cropping implementation from ADM.
    https://github.com/openai/guided-diffusion/blob/8fb3ad9197f16bbc40620447b2742e13458d2831/guided_diffusion/image_datasets.py#L126
    """
    while min(*pil_image.size) >= 2 * image_size:
        pil_image = pil_image.resize(
            tuple(x // 2 for x in pil_image.size), resample=Image.BOX
        )

    scale = image_size / min(*pil_image.size)
    pil_image = pil_image.resize(
        tuple(round(x * scale) for x in pil_image.size), resample=Image.BICUBIC
    )

    arr = np.array(pil_image)
    crop_y = (arr.shape[0] - image_size) // 2
    crop_x = (arr.shape[1] - image_size) // 2
    return Image.fromarray(arr[crop_y: crop_y + image_size, crop_x: crop_x + image_size])


def forward_step(data_iterator, model):
    """Forward step."""
    timers = get_timers()

    # Get the batch.
    timers("batch-generator", log_level=2).start()
    (
        x_start,
        y
    ) = get_batch(data_iterator)
    timers("batch-generator").stop()

    if mpu.is_pipeline_first_stage():
        t = torch.randint(0, diffusion.num_timesteps, (x_start.shape[0],), device=torch.cuda.current_device())
        noise = torch.randn_like(x_start)
        x_t = diffusion.q_sample(x_start, t, noise=noise)
        output_tensor = model(x_start, x_t, t, y, noise)
    else:
        output_tensor = model()
        t = output_tensor[2].to(torch.int64)
        noise = output_tensor[3]
        if mpu.is_pipeline_last_stage():
            x_t = diffusion.q_sample(x_start, t, noise=noise)

    if diffusion.loss_type == LossType.KL or diffusion.loss_type == LossType.RESCALED_KL:
        B, C = x_start.shape[:2]
        assert t.shape == (B,)

    return output_tensor, partial(loss_func, x_start, x_t, t, y, noise)


def build_train_valid_datasets(data_path):
    args = get_args()

    train_transform = transforms.Compose([
        transforms.Lambda(lambda pil_image: center_crop_arr(pil_image, args.image_size)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5], inplace=True)
    ])

    # training dataset
    train_data_path = data_path[0]
    train_data = ImageFolder(
        root=train_data_path,
        transform=train_transform,
    )
    train_data = RandomSeedDataset(train_data)

    # validation dataset
    '''
    val_data_path = data_path[1]
    val_data = ImageFolder(
        root=val_data_path,
        transform=train_transform
    )
    val_data = RandomSeedDataset(val_data)
    '''
    return train_data


def train_valid_test_datasets_provider(train_val_test_num_samples):
    """Build train, valid, and test datasets."""
    args = get_args()

    print_rank_0(
        "> building train, validation, and test datasets " "for VIT ..."
    )
    train_ds = build_train_valid_datasets(
        data_path=args.data_path,
    )
    print_rank_0("> finished creating VIT datasets ...")

    return train_ds, None, None


if __name__ == "__main__":
    train_valid_test_datasets_provider.is_distributed=True
    
    pretrain(
        train_valid_test_datasets_provider,
        model_provider,
        ModelType.dit_block,
        forward_step,
        args_defaults={'dataloader_type': 'cyclic', 'vision_pretraining': True}
    )
