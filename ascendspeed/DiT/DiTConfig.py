# Copyright (c) 2024, NVIDIA CORPORATION. All rights reserved.

from dataclasses import dataclass

from megatron.core.transformer import TransformerConfig


@dataclass
class DiTConfig(TransformerConfig):
    """Configuration object for megatron-core transformers.

    The initialization function has an argument for each parameter, including those in ModelParallelConfig.
    """

    ####################
    # model architecture
    ####################
    frequency_embedding_size: int = 256

    dropout_prob: float = 0.1
 
    num_classes: int = 1000

    input_size: int = 32

    patch_size: int = 4

    in_channels: int = 4

    depth: int = 28

    mlp_ratio: float = 4.0

    class_dropout_prob: float = 0.1

    learn_sigma: bool = True

    latent_size: int = None

    out_channels: int = None

    def __post_init__(self):
        """ Python dataclass method that is used to modify attributes after initialization.
            See https://docs.python.org/3/library/dataclasses.html#post-init-processing for more details.
        """
        super().__post_init__()
        if self.out_channels is None:
            self.out_channels = self.in_channels * 2 if self.learn_sigma else self.in_channels